#include <avr/io.h>
#include <avr/interrupt.h>

// NOTE: Make sure to adjust F_CPU required for util/delay

#define F_CPU 12000000
#include <util/delay.h>

#include "NRF24L01Config.h"
#include "../common/NRF24L01P.h"


// MACROS
#define TRANSFER_SIZE 32
///////////////////////////////////////////////////////////////////////////////
uint8_t data[TRANSFER_SIZE] = "Hello !! How are you man?";

void UART_init()
{
    // Formula for noraml asynchronous mode
    // UBRR = F_OSC/(16*BAUD)  -  1

    // F_CPU 12000000
    // BAUD 19200
    // UBRR 38.0625

    // F_CPU 12000000
    // BAUD 9600
    // UBRR 77.125

    UBRRL = 77;  // set prescaler for baud rate
    UCSRB |= 0x08; // enable transmitter
}

void UART_Transmit( unsigned char data )
{
    while ( ! ( UCSRA &  ( 1 << UDRE) ) );
    UDR = data;
}

void printInt( int data )
{
    char buf[5]; // long enough for largest number
    signed char counter = 0;

    if ( data < 0 )
    {
        data *= -1;
        UART_Transmit( '-' );
    }

    if (data == 0)
        buf[counter++] = '0';

    for ( ; data; data /= 10)
    {
        buf[counter++] = '0' + data%10;
    }

    counter --;
    while ( counter >= 0)
        UART_Transmit(buf[counter--]);
}

class SPI
{
public:
    SPI() {
        // Set MOSI and SCK pin as output
        DDRB |= (1<<5)|(1<<3);

        // Master
        // double speed
        // SPI running at F_CPU/2 ( 6MHz)
        // Enable SPE
        SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPI2X);
    }

    unsigned char writeRead( unsigned char data ) {
        SPDR = data;
        while(!(SPSR & (1<<SPIF) ));
        return(SPDR);
    }

    SPI( const SPI& other) = delete;
    SPI& operator =( const SPI& other) = delete;
    SPI& operator =( const SPI&& other) = delete;
    SPI( const SPI&& other) = delete;
};

void delayInSeconds( uint8_t inSeconds)
{
    uint8_t delayCountFor10ms = inSeconds*100;
    for( int i = 0; i <delayCountFor10ms; ++i)
        _delay_ms(10);
}

NRF24L01P<SPI>*  theRfPtr;
int main()
{
    UART_init();

    // NRF24l01+ output configuration
    // enable CSN(PB2) as output
    DDRB |= (1<<2);

    // enable CE(PD5) as output
    DDRD |= (1<<5);

    SPI spi;
    NRF24L01P<SPI> theRf(&spi);
    theRfPtr = &theRf;
    // Inititalisation process for TX
    {
        // clear all pending interrupts
        theRf.setRegister(REG_STATUS,
                          STATUS_MAX_RT|
                          STATUS_TX_DS|
                          STATUS_RX_DR);   // Clear any pending interrupts


        theRf.setRegister(REG_CONFIG,
                          CONFIG_EN_CRC |
                          CONFIG_MASK_RX_DR |
                          CONFIG_PWR_UP);

        // Enable AutoAcknowledgement for pipe 0
        theRf.setRegister(REG_EN_AA, 0x01);

        // enable only first rx pipe
        // we want only one to one communication
        theRf.setRegister( REG_EN_RXADDR, 0x01 );

        // set transfer size
        // of 32 bytes, it can be between 1-32
        // it is not required at TX Side
        // It is only required at RX Side
//        theRf.setRegister(REG_RX_PW_P0, TRANSFER_SIZE );
    }

//    theRf.powerUp( true );
//    theRf.setTransmitMode();
    theRf.enable( true );

    // configure INT1 falling edge
    MCUCR |= 1 << ISC11;

    // enable external interrupt INT1
    GICR |= 1<<INT1;	// Enable External Interrupt 0
    sei();

    while ( 1 )
    {
        delayInSeconds(1);
        theRf.write(data, TRANSFER_SIZE);
    }
}

ISR(INT1_vect)
{
    uint8_t status = theRfPtr->getStatusRegister();
    if ( status & STATUS_TX_DS)
    {
        theRfPtr->setRegister(REG_STATUS, STATUS_TX_DS);
        UART_Transmit('T');
    }
    else if ( status & STATUS_MAX_RT)
    {
        theRfPtr->setRegister(REG_STATUS, STATUS_MAX_RT);
        theRfPtr->flushTxFifo();
        UART_Transmit('R');
    }
}
