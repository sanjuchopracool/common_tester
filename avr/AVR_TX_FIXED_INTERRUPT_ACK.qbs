import qbs 1.0
import qbs.FileInfo

Product {
    type: ["application", "hex"]
    Depends { name: "cpp" }

    Rule {
        id: hex
        inputs: ["application"]
        prepare: {
            var args = ["-O", "ihex", input.filePath, output.filePath];
            var objcopyPath = "/Users/sanju/opt/gcc-arm-none-eabi-5_4-2016q3/bin/arm-none-eabi-objcopy"
            var cmd = new Command(objcopyPath, args);
            cmd.description = "converting to hex: " + FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;

        }
        Artifact {
            fileTags: ["hex"]
            filePath: FileInfo.baseName(input.filePath) + ".hex"
        }
    }

    Rule {
        id: bin
        inputs: ["application"]
        prepare: {
            var objcopyPath = "/Users/sanju/opt/gcc-arm-none-eabi-5_4-2016q3/bin/arm-none-eabi-objcopy"
            var args = ["-O", "binary", input.filePath, output.filePath];
            var cmd = new Command(objcopyPath, args);
            cmd.description = "converting to bin: "+ FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;

        }
        Artifact {
            fileTags: ["bin"]
            filePath: FileInfo.baseName(input.filePath) + ".bin"
        }
    }

    Rule {
        id: size
        inputs: ["application"]
        alwaysRun: true
        prepare: {
            var sizePath = "/Users/sanju/opt/gcc-arm-none-eabi-5_4-2016q3/bin/arm-none-eabi-size"
            var args = [input.filePath];
            var cmd = new Command(sizePath, args);
            cmd.description = "File size: " + FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;
        }
        Artifact {
            fileTags: ["size"]
            filePath: undefined
        }
    }

    cpp.includePaths: [ ".", "../common/"]
    files: [
        "*.h",
        "TX_FIXED_INTERRUPT_ACK.cpp",
        "*.txt",
        "../common/NRF24L01P.h"
    ]


    cpp.positionIndependentCode: false
    cpp.driverFlags : ["-mmcu=atmega8"]
    cpp.commonCompilerFlags : [ "-g",  "-Os"]
    cpp.cLanguageVersion : "c11"
    cpp.cxxLanguageVersion: "c++11"
    cpp.assemblerFlags : [ "-x","assembler-with-cpp" ]

    cpp.linkerFlags : ["-Os"]
    cpp.visibility: "undefined"
    cpp.warningLevel: "none"
    cpp.debugInformation: false
    cpp.enableExceptions : false
    cpp.enableRtti : false
    cpp.allowUnresolvedSymbols: false
    cpp.optimization: "None"
}
