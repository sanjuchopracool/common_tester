import qbs 1.0

Product {
    type: "application"
    name: "AVR_TX_FIXED.elf"
    consoleApplication: true

    Depends { name: "cpp" }

    cpp.includePaths: [ ".", "../common/"]
    files: [
        "*.h",
        "AVR_TX_FIXED.cpp",
        "*.txt",
        "../common/NRF24L01P.h"
    ]

    cpp.architecture: "avr"


    cpp.positionIndependentCode: false
    cpp.commonCompilerFlags : [ "-g",  "-Os",  "-mmcu=atmega8", "-std=c++11" ]
    cpp.linkerFlags : [ "-g",  "-Os",  "-mmcu=atmega8" ]
    cpp.visibility: "undefined"
    cpp.warningLevel: "none"
    cpp.debugInformation: false
    cpp.enableExceptions : false
    cpp.enableRtti : false
    cpp.allowUnresolvedSymbols: false
    cpp.optimization: "None"
}
