#ifndef NRF24L01_CONFIG_H
#define NRF24L01_CONFIG_H

// These macros should be specific to microcontroller
extern void delayMicroseconds(uint32_t us);

extern void EnableSlave();
extern void DisableSlave();
extern void EnableChip();
extern void DisableChip();
// NOTE: Product specification
// Section: 8.3.1 SPI commands
// Every new command must be started by a high to low transition on CSN

// drop CSN to enable SPI communication
#define NRF24L01P_DropCSN()  EnableSlave()
// raise CSN to disable SPI communication
#define NRF24L01P_RaiseCSN() DisableSlave()

// NOTE: Check section 6.1.6
// raise CE to initiate RF Transmission
#define NRF24L01P_RaiseCE() EnableChip()
// drop CE to disable RF Transmission
#define NRF24L01P_DropCE()  DisableChip()

// Function to delay in microseconds
#define NRF2401LP_DelayUs( x ) delayMicroseconds(x)
#endif // NRF24L01_CONFIG_H
