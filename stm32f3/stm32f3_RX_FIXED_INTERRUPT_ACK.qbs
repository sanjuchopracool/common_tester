import qbs 1.0
import qbs.FileInfo

Product {
    type: ["application", "hex", "bin", "size"]
    Depends { name: "cpp" }

    Rule {
        id: hex
        inputs: ["application"]
        prepare: {
            var args = ["-O", "ihex", input.filePath, output.filePath];
            var objcopyPath = input.cpp.objcopyPath
            var cmd = new Command(objcopyPath, args);
            cmd.description = "converting to hex: " + FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;

        }
        Artifact {
            fileTags: ["hex"]
            filePath: FileInfo.baseName(input.filePath) + ".hex"
        }
    }

    Rule {
        id: bin
        inputs: ["application"]
        prepare: {
            var objcopyPath = input.cpp.objcopyPath
            var args = ["-O", "binary", input.filePath, output.filePath];
            var cmd = new Command(objcopyPath, args);
            cmd.description = "converting to bin: "+ FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;

        }
        Artifact {
            fileTags: ["bin"]
            filePath: FileInfo.baseName(input.filePath) + ".bin"
        }
    }

    Rule {
        id: size
        inputs: ["application"]
        alwaysRun: true
        prepare: {
            var sizePath = input.cpp.toolchainPrefix + "size"
            var args = [input.filePath];
            var cmd = new Command(sizePath, args);
            cmd.description = "File size: " + FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;
        }
        Artifact {
            fileTags: ["size"]
            filePath: undefined
        }
    }

    FileTagger {
        patterns: "*.ld"
        fileTags: ["linkerscript"]
    }

    cpp.defines: [
        "PRINTF_LONG_SUPPORT",
        "STM32F303xC",
        "STM32F3DISCOVERY",
        "USE_FULL_ASSERT",
        "USE_SPI2",
        "USE_STDPERIPH_DRIVER"
    ]
    cpp.includePaths: [ ".",
        "./common/",
        "/Users/sanju/Downloads/STM32F30x_DSP_StdPeriph_Lib_V1.2.3/Libraries/CMSIS/Device/ST/STM32F30x/Include",
        "/Users/sanju/Downloads/STM32F30x_DSP_StdPeriph_Lib_V1.2.3/Libraries/CMSIS/Include",
        "/Users/sanju/Downloads/STM32F30x_DSP_StdPeriph_Lib_V1.2.3/Libraries/STM32F30x_StdPeriph_Driver/inc"
    ]

    files: [
        "*.h",
        "SPI.cpp",
        "RX_FIXED_INTERRUPT_ACK.cpp",
        "*.c",
        "*.s",
        "*.txt",
        "*.ld",
        "/Users/sanju/Downloads/STM32F30x_DSP_StdPeriph_Lib_V1.2.3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_rcc.c",
        "/Users/sanju/Downloads/STM32F30x_DSP_StdPeriph_Lib_V1.2.3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_gpio.c",
        "/Users/sanju/Downloads/STM32F30x_DSP_StdPeriph_Lib_V1.2.3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_spi.c",
        "/Users/sanju/Downloads/STM32F30x_DSP_StdPeriph_Lib_V1.2.3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_usart.c",
        "/Users/sanju/Downloads/STM32F30x_DSP_StdPeriph_Lib_V1.2.3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_exti.c",
        "/Users/sanju/Downloads/STM32F30x_DSP_StdPeriph_Lib_V1.2.3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_syscfg.c",
        "/Users/sanju/Downloads/STM32F30x_DSP_StdPeriph_Lib_V1.2.3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_misc.c",
    ]


//    cpp.cLanguageVersion: "c99"
    cpp.cxxLanguageVersion: "c++11"
    cpp.positionIndependentCode: false
    cpp.optimization: "none"
    cpp.debugInformation: true
    cpp.enableExceptions: false
    cpp.enableRtti: false
    cpp.enableReproducibleBuilds: true
    cpp.treatSystemHeadersAsDependencies: true

    cpp.driverFlags:
        [
        "-mcpu=cortex-m4",
        "-mthumb",
        "-mfloat-abi=softfp",
        "-mfpu=fpv4-sp-d16",
        "-Os",
        "-ffunction-sections",
        "-fdata-sections",
        "-nodefaultlibs",
        "-Wdouble-promotion",
        "-Wall",
        "-flto"
    ]

    cpp.cxxFlags: [
    ]

    cpp.linkerFlags: [
        "--gc-sections",
        "-u,Reset_Handler"
    ]
}
