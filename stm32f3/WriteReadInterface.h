#ifndef WRITEREADINTERFACE_H
#define WRITEREADINTERFACE_H

#include <inttypes.h>

class WriteReadInterface
{
public:
    virtual uint8_t writeRead( uint8_t inData ) = 0;
};

#endif // WRITEREADINTERFACE_H
