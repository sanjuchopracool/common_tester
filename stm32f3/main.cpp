/**
  ******************************************************************************
  * @file    SysTick/SysTick_Example/main.c
  * @author  MCD Application Team
  * @version V1.1.2
  * @date    14-August-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "system.h"
#include "serial.h"
#include <stdbool.h>
#include "SPI.h"
#include "NRF24L01Config.h"
#include "../common/NRF24L01P.h"

// MACROS
#define TRANSFER_SIZE 10
///////////////////////////////////////////////////////////////////////////////

// variables
GPIO_InitTypeDef        GPIO_InitStructure;
static bool isSet = false;
uint8_t data[TRANSFER_SIZE];
NRF24L01P<SPIInterface>* theRfPtr;
///////////////////////////////////////////////////////////////////////////////

//xQueueHandle xQueue;

void toggleLED()
{
    if(isSet)
        GPIO_ResetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
    else
        GPIO_SetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
    isSet = !isSet;
}

void error()
{
    while(1)
    {
        if(isSet)
            GPIO_ResetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        else
            GPIO_SetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        isSet = !isSet;
        delay( 100 );
    }
}

#ifdef STM32F3DISCOVERY
#define NRF24L01P_CS_PIN                        GPIO_Pin_12
#define NRF24L01P_CS_GPIO_PORT                  GPIOB

#define NRF24L01P_CE_PIN                        GPIO_Pin_11
#define NRF24L01P_CE_GPIO_PORT                  GPIOB

#define NRF24L01P_IRQ_PIN                        GPIO_Pin_10
#define NRF24L01P_IRQ_GPIO_PORT                  GPIOB
#define NRF24L01P_IRQ_PORT_SOURCE                EXTI_PortSourceGPIOB
#define NRF24L01P_IRQ_PIN_SOURCE                 EXTI_PinSource10
#define NRF24L01P_IRQ_EXT_LINE                   EXTI_Line10
#define NRF24L01P_CS_CLK_Enable()               RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB , ENABLE )
#define NRF24L01P_CE_CLK_Enable()               RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE )
#define NRF24L01P_IRQ_CLK_Enable()               RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE )
#endif

void EnableSlave()
{
    GPIO_ResetBits( NRF24L01P_CS_GPIO_PORT, NRF24L01P_CS_PIN );
}

void DisableSlave()
{
    GPIO_SetBits( NRF24L01P_CS_GPIO_PORT, NRF24L01P_CS_PIN );
}

void EnableChip()
{
    GPIO_SetBits( NRF24L01P_CE_GPIO_PORT, NRF24L01P_CE_PIN );
}

void DisableChip()
{
    GPIO_ResetBits( NRF24L01P_CE_GPIO_PORT, NRF24L01P_CE_PIN );
}

int main(void)
{
    // it will start sysTick

    systemInit();

    initSerial();

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOE, ENABLE );

    /* Configure PE14 and PE15 in output pushpull mode */
    GPIO_InitStructure.GPIO_Pin = (GPIO_Pin_15 |GPIO_Pin_14 |GPIO_Pin_13 |GPIO_Pin_12 |GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9 | GPIO_Pin_8);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    printf( (char*)"Printf supported !\n" );
//    initPwm(); // keep motor at zero

    // enable CS, CE, IRQ pin clock
    NRF24L01P_CS_CLK_Enable();
    NRF24L01P_CE_CLK_Enable();
    NRF24L01P_IRQ_CLK_Enable();

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    // init pin
    GPIO_InitTypeDef theGPIOInit;

    // configure Slave Select
    theGPIOInit.GPIO_Pin = NRF24L01P_CS_PIN;
    theGPIOInit.GPIO_Mode = GPIO_Mode_OUT;
    theGPIOInit.GPIO_OType = GPIO_OType_PP;
    theGPIOInit.GPIO_Speed = GPIO_Speed_50MHz;
    theGPIOInit.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init( NRF24L01P_CS_GPIO_PORT, &theGPIOInit );
    // CS

    // CE
    theGPIOInit.GPIO_Pin = NRF24L01P_CE_PIN;
    theGPIOInit.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init( NRF24L01P_CE_GPIO_PORT, &theGPIOInit );

    // IRQ
    theGPIOInit.GPIO_Pin = NRF24L01P_IRQ_PIN;
    theGPIOInit.GPIO_Mode = GPIO_Mode_IN;
    theGPIOInit.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init( NRF24L01P_IRQ_GPIO_PORT, &theGPIOInit );

    SPIInterface theSPI( SPIInterface::SPI_2);
    theSPI.Init();
    delay( 100 );

    NRF24L01P<SPIInterface> nrf24(&theSPI);
    printf("%d\n", nrf24.getRegister(0));
    printf("%d\n", nrf24.getRegister(1));
    printf("%d\n", nrf24.getRegister(2));
    printf("%d\n", nrf24.getRegister(3));
    nrf24.powerUp( true );
    nrf24.setReceiveMode();
    nrf24.enable( true );

    while ( 1 )
    {
        if ( nrf24.readable() )
        {
            nrf24.read( data, 10 );
            for ( int i = 0; i < 10; ++i)
                printf("%d",data[i] );
            printf("\n");
        }
    }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    error();
}
#endif

void EXTIRQHandler()
{
    if( GPIO_ReadOutputDataBit(GPIOB, GPIO_Pin_10) == RESET )
    {
//        EXTI_ClearITPendingBit(EXTI_Line10);
//        theRfPtr->read(NRF24L01P::RX_PIPE::PIPE_0, data, TRANSFER_SIZE);
//        if( 'A' == data[0] && 'A' == data[1] && 'A' == data[2] && 'Z' == data[9] )
//        {
//            throttle = (((int32_t)((data[3] << 8) | data[4])*100) >> 16);
//            pitch = (((int32_t)((data[7] << 8) | data[8])*100) >> 16);
//            roll = (((int32_t)((data[5] << 8) | data[6])*100) >> 16);
//            printf((char*)"%d, %d, %d\n", (int16_t)throttle, (int16_t)pitch, (int16_t)roll);
//            handleSingleMotor();
//        }
    }
}
