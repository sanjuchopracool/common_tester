import qbs 1.0

Product {
    type: "application"
    name: "stm32f3.elf"
    consoleApplication: true

    Depends { name: "cpp" }

    cpp.defines: [
        "PRINTF_LONG_SUPPORT",
        "STM32F303xC",
        "STM32F3DISCOVERY",
        "USE_FULL_ASSERT",
        "USE_SPI2",
        "USE_STDPERIPH_DRIVER"
    ]
    cpp.includePaths: [ ".",
        "./common/",
        "/home/sanju/stm/STM32Cube/STPL-F3/Libraries/CMSIS/Device/ST/STM32F30x/Include",
        "/home/sanju/stm/STM32Cube/STPL-F3/Libraries/CMSIS/Include",
        "/home/sanju/stm/STM32Cube/STPL-F3/Libraries/STM32F30x_StdPeriph_Driver/inc"
    ]

    files: [
        "*.h",
        "SPI.cpp",
        "RX_FIXED_INTERRUPT.cpp",
        "*.c",
        "*.s",
        "*.txt",
        "/home/sanju/stm/STM32Cube/STPL-F3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_rcc.c",
        "/home/sanju/stm/STM32Cube/STPL-F3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_gpio.c",
        "/home/sanju/stm/STM32Cube/STPL-F3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_spi.c",
        "/home/sanju/stm/STM32Cube/STPL-F3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_usart.c",
        "/home/sanju/stm/STM32Cube/STPL-F3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_exti.c",
        "/home/sanju/stm/STM32Cube/STPL-F3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_syscfg.c",
        "/home/sanju/stm/STM32Cube/STPL-F3/Libraries/STM32F30x_StdPeriph_Driver/src/stm32f30x_misc.c",
    ]

    cpp.architecture: "armv4t"


    cpp.positionIndependentCode: false
    cpp.commonCompilerFlags : [
        "-mcpu=cortex-m4",
        "-mthumb",
        "-mfloat-abi=softfp",
        "-mfpu=fpv4-sp-d16",
        "-std=c++11",
        "-Os",
        "-ffunction-sections",
        "-fdata-sections",
        "-Wall",
    ]

    cpp.linkerFlags : [
        "-mcpu=cortex-m4",
        "-mthumb",
        "-mfloat-abi=softfp",
        "-mfpu=fpv4-sp-d16",
        "-T/home/sanju/Desktop/common_tester/stm32f3/STM32F303VC_FLASH.ld",
        "-Wl,--start-group",
        "-lc",
        "-lm",
        "-Wl,--end-group",
        "-static",
        "-Wl,-u,Reset_Handler",
        "-Wl,--gc-sections"
    ]
    cpp.visibility: "undefined"
    cpp.warningLevel: "none"
    cpp.debugInformation: false
    cpp.enableExceptions : false
    cpp.enableRtti : false
    cpp.allowUnresolvedSymbols: false
    cpp.optimization: "None"
}
